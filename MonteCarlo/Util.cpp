#include "Util.h"
#include <vector>
#include "glm/vec3.hpp"
#include"glm/gtc/type_ptr.hpp"

#include <iostream>
#include <math.h>

CUtil::CUtil()
{

}

CUtil::~CUtil()
{

}

glm::vec3 CUtil::CrossProduct(glm::vec3 a, glm::vec3 b)
{
	glm::vec3 crossResult;
	crossResult.x = a.y * b.z - a.z * b.y;
	crossResult.y = a.z * b.x - a.x * b.z;
	crossResult.z = a.x * b.y - a.y * b.x;
	return crossResult;
}

float CUtil::DotProduct(glm::vec3 a, glm::vec3 b)
{
	return a.x * b.x + a.y * b.y + a.z * b.z;
}
bool CUtil::IsSame(glm::vec3 p, glm::vec3 a, glm::vec3 b, glm::vec3 c)
{
	glm::vec3 ab = b - a;
	glm::vec3 ac = c - a;
	glm::vec3 ap = p - a;

	glm::vec3 abXac;
	glm::vec3 abXap;
	abXac = CrossProduct(ab, ac);
	abXap = CrossProduct(ab, ap);

	if (DotProduct(abXac, abXap) > 0)
		return true;

	return false;


}

bool CUtil::ComputePointInTriangle(glm::vec3 p, glm::vec3 a, glm::vec3 b, glm::vec3 c)
{
	if (IsSame(p, a, b, c) && IsSame(p, b, c, a) && IsSame(p, c, a, b))
		return true;


	return false;


}


bool CUtil::IsLine(glm::vec3 p, glm::vec3 a, glm::vec3 b)
{
	glm::vec3 minPos;
	glm::vec3 maxPos;
	if (a.x <= b.x)
	{
		minPos.x = a.x;
		maxPos.x = b.x;
	}
	else
	{
		minPos.x = b.x;
		maxPos.x = a.x;
	}
	if (a.y <= b.y)
	{
		minPos.y = a.y;
		maxPos.y = b.y;
	}
	else
	{
		minPos.y = b.y;
		maxPos.y = a.y;
	}
	if (a.z <= b.z)
	{
		minPos.z = a.z;
		maxPos.z = b.z;
	}
	else
	{
		minPos.z = b.z;
		maxPos.z = a.z;
	}


	if (p.x >= minPos.x && p.x <= maxPos.x)
		if (p.y >= minPos.y && p.y <= maxPos.y)
			if (p.z >= minPos.z && p.z <= maxPos.z)
			{
				glm::vec3 ab = b - a;
				glm::vec3 ap = p - a;
				glm::vec3 abN = normalize(ab);
				glm::vec3 apN = normalize(ap);
				if (DotProduct(abN, apN) <= 1.00001 && DotProduct(abN, apN) >= 0.99999)
					return true;
			}
	return false;
		
	
}
bool CUtil::ComputePointOnTriangle(glm::vec3 p, glm::vec3 a, glm::vec3 b, glm::vec3 c)
{
	//std::cout << IsLine(p, a, b) << std::endl;
	//std::cout << IsLine(p, b, c) << std::endl;
	//std::cout << IsLine(p, c, a) << std::endl;
	if (IsLine(p, a, b) || IsLine(p, b, c) || IsLine(p, c, a))
		return true;

	return false;


}
float CUtil::GetVectorSize(glm::vec3 a)
{
	return sqrt(pow(a.x, 2) + pow(a.y, 2) + pow(a.z, 2));
}

float CUtil::Norm2(glm::vec3 v)
{
	return v.x * v.x + v.y * v.y + v.z * v.z;
}

bool CUtil::LineToLineIntersection(glm::vec3 a1, glm::vec3 a2, glm::vec3 b1, glm::vec3 b2, glm::vec3 &iP)
{
	glm::vec3 da = a2 - a1;
	glm::vec3 db = b2 - b1;
	glm::vec3 dc = b1 - a1;

	if (dot(dc, cross(da, db)) != 0.0) // lines are not coplanar
		return false;

	float s = dot(cross(dc, db), cross(da, db)) / Norm2(cross(da, db));
	if (s >= 0.0 && s <= 1.0)
	{
		iP = a1 + da * s;
		return true;
	}
	return false;

}








//VecToDouble
/*
vector a를 double b로 변환
*/
void CUtil::VecToDouble(glm::vec3 a, double b[])
{
	b[0] = a.x;
	b[1] = a.y;
	b[2] = a.z;

}

//DoubleToVec
/*
double a를 vector로 return
*/
glm::vec3 CUtil::DoubleToVec(double a[])
{
	glm::vec3 b;
	b.x = a[0];
	b.y = a[1];
	b.z = a[2];

	return b;

}
