#pragma once
#ifndef UTIL_H
#define UTIL_H


#include <vector>
#include "glm/vec3.hpp"



class CUtil
{
private:
	static bool IsSame(glm::vec3 p, glm::vec3 a, glm::vec3 b, glm::vec3 c);
	static float Norm2(glm::vec3 v);

public:
	glm::vec3			pos;
	CUtil();
	~CUtil();

	static bool IsLine(glm::vec3 p, glm::vec3 a, glm::vec3 b);
	static bool ComputePointInTriangle(glm::vec3 p, glm::vec3 a, glm::vec3 b, glm::vec3 c);
	static bool ComputePointOnTriangle(glm::vec3 p, glm::vec3 a, glm::vec3 b, glm::vec3 c);

	static bool LineToLineIntersection(glm::vec3 a, glm::vec3 b, glm::vec3 c, glm::vec3 d, glm::vec3 &iP);

	static float GetVectorSize(glm::vec3 a);
	static glm::vec3 CrossProduct(glm::vec3 a, glm::vec3 b);
	static float DotProduct(glm::vec3 a, glm::vec3 b);



	static bool PairCompareDV(const std::pair<double, glm::vec3> & a, const std::pair<double, glm::vec3> & b)
	{
		if (a.first == b.first)
		{
			return a.second.x < b.second.x;
		}
		return a.first < b.first;
	}
	static bool PairCompareII(const std::pair<int, int> & a, const std::pair<int, int> & b)
	{
		if (a.first == b.first)
		{
			return a.second < b.second;
		}
		return a.first < b.first;
	}



	static void VecToDouble(glm::vec3 a, double b[]);
	static glm::vec3 DoubleToVec(double a[]);

	bool operator ==(glm::vec3 &v)
	{
		if (pos.x == v.x && pos.y == v.y && pos.z == v.z)
			return true;
		return false;
	}
};


















struct float3
{
	float fValue[3];

	float3() : fValue{ 0.f, } {};

	float3(float a, float b, float c)
		: fValue{ a,b,c }
	{};

	explicit float3(const float a[3])
		: fValue{ a[0], a[1], a[2] }
	{};

	float3(const float3& rhs)
		: fValue{ rhs.fValue[0], rhs.fValue[1], rhs.fValue[2] }
	{};

	float3& operator = (const float af[3])
	{
		fValue[0] = af[0];		fValue[1] = af[1];		fValue[2] = af[2];
		return (*this);
	};
	float3& operator = (const float3& rhs)
	{
		fValue[0] = rhs.fValue[0];		fValue[1] = rhs.fValue[1];		fValue[2] = rhs.fValue[2];
		return (*this);
	};
	float3 operator + (const float3& rhs) const
	{
		float3 fSum(fValue[0] + rhs.fValue[0], fValue[1] + rhs.fValue[1], fValue[2] + rhs.fValue[2]);
		return fSum;
	};
	float3 operator - (const float3& rhs) const
	{
		float3 fMinus(fValue[0] - rhs.fValue[0], fValue[1] - rhs.fValue[1], fValue[2] - rhs.fValue[2]);
		return fMinus;
	};
	float3 operator - () const
	{
		float3 fMinus(-fValue[0], -fValue[1], -fValue[2]);
		return fMinus;
	};
	float3 operator * (const float3& rhs) const
	{
		float3 fTimes(fValue[0] * rhs.fValue[0], fValue[1] * rhs.fValue[1], fValue[2] * rhs.fValue[2]);
		return fTimes;
	};
	float3 operator * (const float fT) const
	{
		float3 fTimes(fValue[0] * fT, fValue[1] * fT, fValue[2] * fT);
		return fTimes;
	};
	float3 operator / (const float3& rhs) const
	{
		float3 fDivide(fValue[0] / rhs.fValue[0], fValue[1] / rhs.fValue[1], fValue[2] / rhs.fValue[2]);
		return fDivide;
	};
	float3 operator / (const float fT) const
	{
		float3 fDivide(fValue[0] / fT, fValue[1] / fT, fValue[2] / fT);
		return fDivide;
	};
	float operator [] (int nIndex) const
	{
		return fValue[nIndex];
	};
	bool operator == (const float3& rhs) const
	{
		if (fValue[0] == rhs.fValue[0] && fValue[1] == rhs.fValue[1] && fValue[2] == rhs.fValue[2])
			return true;

		return false;
	};
	bool operator != (const float3& rhs) const
	{
		return !(*this == rhs);
	};
	float Magnitude(void) const
	{
		return sqrtf(fValue[0] * fValue[0] + fValue[1] * fValue[1] + fValue[2] * fValue[2]);
	};
	float3 Normalize(void)
	{
		const double norm = Magnitude();

		if (norm < FLT_EPSILON)
		{
			fValue[0] = fValue[1] = fValue[2] = 0.f;
		}
		else
		{
			fValue[0] = static_cast<float>((double)fValue[0] / norm);
			fValue[1] = static_cast<float>((double)fValue[1] / norm);
			fValue[2] = static_cast<float>((double)fValue[2] / norm);
		}
		return (*this);
	};
	static float GetDistance(float3 p1, float3 p2)
	{
		float3 fSub = p1 - p2;
		float3 fTimes = fSub*fSub;
		return sqrtf(fTimes[0] + fTimes[1] + fTimes[2]);
	}
};

#endif