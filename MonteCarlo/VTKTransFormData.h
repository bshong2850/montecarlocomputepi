#pragma once
#ifndef VTKTRANSFORMDATA_H
#define VTKTRANSFORMDATA_H

#include "vtkSmartPointer.h"
#include <vector>
#include "glm/vec3.hpp"

class vtkActor;
class vtkPolyData;
class vtkIdList;






class VTKTransFormData
{
public:
	static vtkSmartPointer<vtkActor> PolyDataToActor(vtkSmartPointer<vtkPolyData> polyData, std::string setColor = "Red");

	static vtkSmartPointer<vtkActor> PointsToPointsVTKActor(vtkSmartPointer<vtkPolyData> polyData, vtkSmartPointer<vtkIdList> pts, float size = 1.0f, std::string setColor = "Red");
	static vtkSmartPointer<vtkActor> PointsToPointsVTKActor(std::vector<glm::vec3> points, float size = 1.0f, std::string color = "Red");
	static vtkSmartPointer<vtkActor> PointsToLinesVTKActor(std::vector<glm::vec3> points, bool closedFlag = false, float size = 1.0f, std::string color = "Red");
	static vtkSmartPointer<vtkPolyData> PointsToLinesVTKPolyData(std::vector<glm::vec3> pts, bool closedFlag = false);
	
	static void WritePolyData(const char * filename, vtkSmartPointer<vtkPolyData> polyData);
	static void ReadPolyData(const char * filename, vtkSmartPointer<vtkPolyData> polyData);
};



#endif