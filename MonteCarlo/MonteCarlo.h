#pragma once
#ifndef MONTECARLO_H
#define MONTECARLO_H


#include <vector>
#include "glm/vec3.hpp"


class MonteCarlo
{
public:
	MonteCarlo() {};
	~MonteCarlo() {};


	void ComputePI();
private:
	double GenerateRandomValue(double minV, double maxV);

};


#endif