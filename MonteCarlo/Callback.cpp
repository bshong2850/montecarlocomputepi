
#pragma once
#include "Callback.h"
#include "VTKTransFormData.h"
#include "vtkInteractorStyle.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkRenderer.h"
#include "vtkRendererCollection.h"
#include <vtkTextProperty.h>
#include <vtkRenderWindow.h>
#include <vtkTextActor.h>
#include <vtkTextWidget.h>
#include <vtkTextRepresentation.h>

#include <Windows.h>
#include <vtkMath.h>
#include <time.h>

#include <math.h>

double MonteCarloKeyPressCallback::ComputeDistance(glm::vec3 a)
{
	return sqrt(pow(a.x, 2) + pow(a.y, 2) + pow(a.z, 2));
}

void MonteCarloKeyPressCallback::SetCubeLength(double cubeLength)
{
	this->cubeLength = cubeLength;
}
void MonteCarloKeyPressCallback::RenderRandomPoint(vtkRenderWindowInteractor *rwi, int pointSize)
{
	vtkMath::RandomSeed(time(NULL)); 
	double limits = cubeLength / 2;
	std::vector<glm::vec3> randomPointVec;
	for (int i = 0; i < pointSize; ++i)
	{
		//정육면체 내부에 랜덤으로 포인트 생성
		glm::vec3 randomPoint = glm::vec3(vtkMath::Random(-limits, limits),
			vtkMath::Random(-limits, limits),
			vtkMath::Random(-limits, limits));

		//만약 구 밖에 점이 찍혔다면
		if (ComputeDistance(randomPoint) > limits)
		{
			//구 바깥쪽 Vector에 저장
			sphereOutsidepointVec.push_back(randomPoint);
		}
		else
		{
			//구 안쪽 Vector에 저장
			sphereInsidepointVec.push_back(randomPoint);
		}
	}

	// 정육면체 안에 찍힌 점의 수
	float totalPointNum = sphereOutsidepointVec.size() + sphereInsidepointVec.size();

	// 구 안에 찍힌 점의 수
	float insidePointNum = sphereInsidepointVec.size();

	// PI 계산
	float vPI = insidePointNum * 6.0 / totalPointNum;


	char s1[10];
	sprintf(s1, "%f", vPI);

	vtkSmartPointer<vtkTextActor> textActor = vtkSmartPointer<vtkTextActor>::New();
	textActor->SetInput(s1);
	textActor->GetTextProperty()->SetColor(1.0, 1.0, 1.0);
	textActor->SetPosition2(10, 40);
	textActor->GetTextProperty()->SetFontSize(34);
	vtkSmartPointer<vtkTextWidget> textWidget =
		vtkSmartPointer<vtkTextWidget>::New();


	textWidget->SetInteractor(rwi);
	textWidget->SetTextActor(textActor);
	textWidget->SelectableOff();
	textWidget->On();
	std::cout << insidePointNum * 6.0 / totalPointNum << " " << totalPointNum  << std::endl;

	vtkSmartPointer<vtkActor> insidePointActor = vtkSmartPointer<vtkActor>::New();
	vtkSmartPointer<vtkActor> outsidePointActor = vtkSmartPointer<vtkActor>::New();
	insidePointActor = VTKTransFormData::PointsToPointsVTKActor(sphereInsidepointVec, 4.0f, "Red");
	outsidePointActor = VTKTransFormData::PointsToPointsVTKActor(sphereOutsidepointVec, 4.0f, "Blue");
	rwi->GetRenderWindow()->GetRenderers()->GetFirstRenderer()->AddActor(insidePointActor);
	rwi->GetRenderWindow()->GetRenderers()->GetFirstRenderer()->AddActor(outsidePointActor);
	rwi->Render();
}
// key가 눌렸을 때 실행되는 Callback 함수
void MonteCarloKeyPressCallback::Execute(vtkObject *caller, unsigned long, void*)
{
	vtkRenderWindowInteractor *rwi = reinterpret_cast<vtkRenderWindowInteractor *>(caller);
	std::string key = rwi->GetKeySym();
	vtkSmartPointer<vtkInteractorStyle> style;

	if (key == "n")
	{
		for(int i = 0; i<300; ++i)
		{
			RenderRandomPoint(rwi, 100000);
			Sleep(100);
		}
	}
}
