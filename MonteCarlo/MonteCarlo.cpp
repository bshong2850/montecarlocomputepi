#include "MonteCarlo.h"
#include "VTKTransFormData.h"
#include "Callback.h"

#include <vtkSmartPointer.h>

#include <vtkActor.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>

#include <vtkSphereSource.h>
#include <vtkCubeSource.h>
#include <vtkPointSource.h>
#include <vtkAppendPolyData.h>
#include <vtkProperty.h>
#include <vtkNamedColors.h>

#include <vtkMath.h>
#include <time.h>
void MonteCarlo::ComputePI()
{
	vtkSmartPointer<vtkNamedColors> colors = vtkSmartPointer<vtkNamedColors>::New();
	vtkSmartPointer<vtkCubeSource> cubeSource =
		vtkSmartPointer<vtkCubeSource>::New();

	double cubeLength = 2.0;
	cubeSource->SetCenter(0.0, 0.0, 0.0);
	cubeSource->SetXLength(cubeLength);
	cubeSource->SetYLength(cubeLength);
	cubeSource->SetZLength(cubeLength);


	// Create a mapper and actor.
	vtkSmartPointer<vtkPolyDataMapper> cubeMapper =
		vtkSmartPointer<vtkPolyDataMapper>::New();
	cubeMapper->SetInputConnection(cubeSource->GetOutputPort());

	vtkSmartPointer<vtkActor> cubeActor =
		vtkSmartPointer<vtkActor>::New();
	cubeActor->SetMapper(cubeMapper);
	cubeActor->GetProperty()->SetColor(colors->GetColor3d("White").GetData());
	cubeActor->GetProperty()->SetOpacity(0.5);
	cubeActor->GetProperty()->EdgeVisibilityOn();
	

	vtkSmartPointer<vtkSphereSource> sphereSource =
		vtkSmartPointer<vtkSphereSource>::New();


	sphereSource->SetCenter(0.0, 0.0, 0.0);
	sphereSource->SetRadius(cubeLength / 2);
	sphereSource->SetPhiResolution(100);
	sphereSource->SetThetaResolution(100);
	// Create a mapper and actor.
	vtkSmartPointer<vtkPolyDataMapper> sphereMapper =
		vtkSmartPointer<vtkPolyDataMapper>::New();
	sphereMapper->SetInputConnection(sphereSource->GetOutputPort());

	vtkSmartPointer<vtkActor> sphereActor =
		vtkSmartPointer<vtkActor>::New();
	sphereActor->SetMapper(sphereMapper);
	sphereActor->GetProperty()->SetColor(colors->GetColor3d("Gold").GetData());
	sphereActor->GetProperty()->SetOpacity(0.5);

	// Create a renderer, render window, and interactor
	vtkSmartPointer<vtkRenderer> renderer =
		vtkSmartPointer<vtkRenderer>::New();
	vtkSmartPointer<vtkRenderWindow> renderWindow =
		vtkSmartPointer<vtkRenderWindow>::New();
	renderWindow->AddRenderer(renderer);
	vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor =
		vtkSmartPointer<vtkRenderWindowInteractor>::New();
	renderWindowInteractor->SetRenderWindow(renderWindow);

	//Callback
	MonteCarloKeyPressCallback *KeyFunction = MonteCarloKeyPressCallback::New();

	KeyFunction->SetCubeLength(cubeLength);
	renderWindowInteractor->AddObserver(vtkCommand::KeyPressEvent, KeyFunction);

	// Add the actors to the scene
	renderer->AddActor(cubeActor);
	//renderer->AddActor(pointActor);
	renderer->AddActor(sphereActor);
	renderer->SetBackground(0.1, 0.2, 0.4);

	// Render and interact
	renderWindow->Render();
	renderWindowInteractor->Start();

	return;

}

double MonteCarlo::GenerateRandomValue(double minV, double maxV)
{
	unsigned int numRand = 3;

	// Without this line, the random numbers will be the same every iteration.
	vtkMath::RandomSeed(time(NULL));

	return vtkMath::Random(minV, maxV);
}


