#include "VTKTransFormData.h"
#include "vtkPolyDataMapper.h"


#include <vtkSTLWriter.h>
#include <vtkSTLReader.h>

#include "vtkActor.h"
#include "vtkPoints.h"
#include "vtkPolyData.h"

#include "vtkVertexGlyphFilter.h"
#include "vtkProperty.h"

#include "vtkLine.h"
#include "vtkCellArray.h"

#include "vtkAppendPolyData.h"
#include "vtkNamedColors.h"


#include "Util.h"






/*
Point들을 받아와서 point actor로 반환
*/
vtkSmartPointer<vtkActor> VTKTransFormData::PolyDataToActor(vtkSmartPointer<vtkPolyData> polyData, std::string setColor)
{
	vtkSmartPointer<vtkPolyDataMapper> mapper =
		vtkSmartPointer<vtkPolyDataMapper>::New();
	mapper->SetInputData(polyData);

	vtkSmartPointer<vtkActor> actor =
		vtkSmartPointer<vtkActor>::New();
	actor->SetMapper(mapper);

	vtkSmartPointer<vtkNamedColors> colors = vtkSmartPointer<vtkNamedColors>::New();
	actor->GetProperty()->SetColor(colors->GetColor3d(setColor).GetData());

	return actor;

}

/*
Point들을 받아와서 point actor로 반환
*/
vtkSmartPointer<vtkActor> VTKTransFormData::PointsToPointsVTKActor(vtkSmartPointer<vtkPolyData> polyData, vtkSmartPointer<vtkIdList> pts, float size, std::string setColor)
{
	vtkSmartPointer<vtkPoints> points =
		vtkSmartPointer<vtkPoints>::New();

	const int numPoints = pts->GetNumberOfIds();
	for (int i = 0; i < numPoints; ++i)
	{
		double x[3];
		polyData->GetPoint(pts->GetId(i), x);
		points->InsertNextPoint(x);
	}

	vtkSmartPointer<vtkPolyData> pointsPolydata =
		vtkSmartPointer<vtkPolyData>::New();

	pointsPolydata->SetPoints(points);

	vtkSmartPointer<vtkVertexGlyphFilter> vertexFilter =
		vtkSmartPointer<vtkVertexGlyphFilter>::New();
	vertexFilter->SetInputData(pointsPolydata);
	vertexFilter->Update();

	vtkSmartPointer<vtkPolyData> point =
		vtkSmartPointer<vtkPolyData>::New();
	point->ShallowCopy(vertexFilter->GetOutput());

	vtkSmartPointer<vtkActor> pointactor =
		vtkSmartPointer<vtkActor>::New();
	pointactor = PolyDataToActor(point, setColor);
	pointactor->GetProperty()->SetPointSize(size);

	return pointactor;

}

//PointsToFacesVTKActor
/*
Point들을 받아와서 point actor로 반환
*/
vtkSmartPointer<vtkActor> VTKTransFormData::PointsToPointsVTKActor(std::vector<glm::vec3> pts, float size, std::string setColor)
{
	vtkSmartPointer<vtkPoints> points =
		vtkSmartPointer<vtkPoints>::New();

	size_t numPoints = pts.size();
	for (int i = 0; i < numPoints; ++i)
	{
		double x[3];
		CUtil::VecToDouble(pts[i], x);
		points->InsertNextPoint(x);
	}

	vtkSmartPointer<vtkPolyData> pointsPolydata =
		vtkSmartPointer<vtkPolyData>::New();

	pointsPolydata->SetPoints(points);

	vtkSmartPointer<vtkVertexGlyphFilter> vertexFilter =
		vtkSmartPointer<vtkVertexGlyphFilter>::New();
	vertexFilter->SetInputData(pointsPolydata);
	vertexFilter->Update();

	vtkSmartPointer<vtkPolyData> point =
		vtkSmartPointer<vtkPolyData>::New();
	point->ShallowCopy(vertexFilter->GetOutput());

	vtkSmartPointer<vtkActor> pointactor =
		vtkSmartPointer<vtkActor>::New();
	pointactor = PolyDataToActor(point, setColor);
	pointactor->GetProperty()->SetPointSize(size);
	return pointactor;

}

//PointsToLinesVTKActor
/*
Point들을 받아와서 line actor로 반환
*/
vtkSmartPointer<vtkActor> VTKTransFormData::PointsToLinesVTKActor(std::vector<glm::vec3> pts, bool closedFlag, float lineWidth, std::string setColor)
{
	vtkSmartPointer<vtkPoints> vtklinePoints =
		vtkSmartPointer<vtkPoints>::New();

	size_t numPoints = pts.size();
	for (int i = 0; i < numPoints; ++i)
	{
		double x[3];
		CUtil::VecToDouble(pts[i], x);
		vtklinePoints->InsertNextPoint(x);
	}

	vtkSmartPointer<vtkCellArray> lines =
		vtkSmartPointer<vtkCellArray>::New();
	vtkSmartPointer<vtkPolyData> linesPolyData =
		vtkSmartPointer<vtkPolyData>::New();
	for (int i = 0; i < numPoints - 1; ++i)
	{
		vtkSmartPointer<vtkLine> line =
			vtkSmartPointer<vtkLine>::New();
		line->GetPointIds()->SetId(0, i);
		line->GetPointIds()->SetId(1, i + 1);
		lines->InsertNextCell(line);
	}
	if (closedFlag)
	{
		vtkSmartPointer<vtkLine> line =
			vtkSmartPointer<vtkLine>::New();
		line->GetPointIds()->SetId(0, numPoints - 1);
		line->GetPointIds()->SetId(1, 0);
		lines->InsertNextCell(line);
	}

	linesPolyData->SetPoints(vtklinePoints);
	linesPolyData->SetLines(lines);

	vtkSmartPointer<vtkActor> lineactor =
		vtkSmartPointer<vtkActor>::New();
	lineactor = PolyDataToActor(linesPolyData, setColor);
	lineactor->GetProperty()->SetLineWidth(lineWidth);

	return lineactor;

}

//PointsToLinesVTKPolyData
/*
Point들을 받아와서 line polyData로 반환
*/
vtkSmartPointer<vtkPolyData> VTKTransFormData::PointsToLinesVTKPolyData(std::vector<glm::vec3> pts, bool closedFlag)
{
	vtkSmartPointer<vtkPoints> vtklinePoints =
		vtkSmartPointer<vtkPoints>::New();

	size_t numPoints = pts.size();
	for (int i = 0; i < numPoints; ++i)
	{
		double x[3];
		CUtil::VecToDouble(pts[i], x);
		vtklinePoints->InsertNextPoint(x);
	}

	vtkSmartPointer<vtkCellArray> lines =
		vtkSmartPointer<vtkCellArray>::New();
	vtkSmartPointer<vtkPolyData> linesPolyData =
		vtkSmartPointer<vtkPolyData>::New();
	for (int i = 0; i < numPoints - 1; ++i)
	{
		vtkSmartPointer<vtkLine> line =
			vtkSmartPointer<vtkLine>::New();
		line->GetPointIds()->SetId(0, i);
		line->GetPointIds()->SetId(1, i + 1);
		lines->InsertNextCell(line);
	}
	if (closedFlag)
	{
		vtkSmartPointer<vtkLine> line =
			vtkSmartPointer<vtkLine>::New();
		line->GetPointIds()->SetId(0, numPoints - 1);
		line->GetPointIds()->SetId(1, 0);
		lines->InsertNextCell(line);
	}

	linesPolyData->SetPoints(vtklinePoints);
	linesPolyData->SetLines(lines);

	return linesPolyData;

}




void VTKTransFormData::WritePolyData(const char * filename, vtkSmartPointer<vtkPolyData> polyData)
{
	std::cout << "Data Write" << std::endl;
	vtkSmartPointer<vtkSTLWriter> stlWriter =
		vtkSmartPointer<vtkSTLWriter>::New();
	stlWriter->SetFileName(filename);
	stlWriter->SetInputData(polyData);
	stlWriter->Write();
	return;
}

void VTKTransFormData::ReadPolyData(const char * filename, vtkSmartPointer<vtkPolyData> polyData)
{
	std::cout << "Data Read" << std::endl;
	vtkSmartPointer<vtkSTLReader> reader =
		vtkSmartPointer<vtkSTLReader>::New();
	reader->SetFileName(filename);
	reader->Update();
	polyData->DeepCopy(reader->GetOutput());
	return;
}
