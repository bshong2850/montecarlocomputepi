#pragma once
#include "vtkCommand.h"


#include "glm/vec3.hpp"
#include <vector>
#include <algorithm>

class vtkRenderWindowInteractor;

class MonteCarloKeyPressCallback : public vtkCommand
{
public:
	static MonteCarloKeyPressCallback *New()
	{
		return new MonteCarloKeyPressCallback;
	}
	virtual void Execute(vtkObject *caller, unsigned long, void*);
	void SetCubeLength(double cubeLength);
	void RenderRandomPoint(vtkRenderWindowInteractor *rwi, int pointSize);
	double ComputeDistance(glm::vec3 a);
protected:
	std::vector<glm::vec3> sphereInsidepointVec;
	std::vector<glm::vec3> sphereOutsidepointVec;
	double cubeLength;
	bool stopFlag = false;;
};
