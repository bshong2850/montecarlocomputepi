Monte Carlo 방법으로 PI(3.141592.....)를 구하는 코드입니다.

실행하시면 VTK를 활용하여 반지름 1의 구와 한변의 길이 2인 Cube가 있습니다.

n을 누르면 Cube 내부의 Random한 위치에 Point가 생성됩니다.





- PI 구하는 법 - 

구 내부에 찍힌 점의 수를 a
Cube내부에 찍힌 점의 수를 b로 두고,
구의 부피는 4/3 * PI * r^3
Cube의 부피는 r^3 이므로

구의 부피 : Cube의 부피 = a : b로 표현이 가능합니다.

따라서 식을 정리해보면 PI = 6 * a / b 가 됩니다.


결론적으로 Random한 곳에 포인트가 찍히고 그 포인트의 위치를 보면서 a와 b를 계산하여 PI를 구해 Console창에 띄웁니다.